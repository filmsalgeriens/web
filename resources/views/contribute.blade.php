@extends('layout')

@section('content')
<h1 class="text-xl flex items-center mono uppercase">
    <span class="text-4xl text-mustard mr-2"><i class="fas fa-angle-right"></i></span> Contribuer
</h1>
<p class="mt-4 text-gray-800">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque neque, nemo beatae architecto, voluptates non, quidem expedita laboriosam dolorum quibusdam adipisci asperiores molestiae ipsa tenetur minus. Iste porro eos animi!
</p>
<ul class="mt-4 link-list inline-block">
    <li>
        <a href="#" class="">
            <span class="icon"> <i class="fas fa-angle-right"></i></span>
            <span class="text">Ajouter un film</span>
        </a>
    </li>
    <li>
        <a href="#" class="">
            <span class="icon"><i class="fas fa-angle-right"></i></span>
            <span class="text">Signalez une erreur</span>
        </a>
    </li>
    <li>
        <a href="#" class="">
            <span class="icon"><i class="fas fa-angle-right"></i></span>
            <span class="text">Contribuer autrement</span>
        </a>
    </li>
</ul>
@endsection