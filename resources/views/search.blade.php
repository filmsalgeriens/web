@extends('layout')

@section('content')
<h1 class="text-xl flex items-center uppercase mono">
    <span class="text-4xl text-mustard mr-2"><i class="fas fa-angle-right"></i></span>Recherche
    @if($films->count())
    <span class="text-xs text-gray-600 ml-10">{{ $films->count() }} résultats</span>
    @endif
</h1>

<form class="mt-10 pb-2 mb-2 border-b border-gray-400 flex items-center" method="GET">
    <span class="text-gray-700"><i class="fas fa-search"></i></span>
    <input type="text" class="p-3 outline-none mono flex-1 mr-4" placeholder="Recherche.." name="q" value="{{ request()->get('q') }}">
    <div class="select_wrapper text-gray-600 hover:text-gray-700 mx-3">
        <div class="select_overlay">
            <span class="mr-3"><i class="fas fa-chevron-down"></i></span>
            <span class="select_label" data-placeholder="Décennie">Genre</span>
        </div>
        <select id="genre" name="genre" class="filter opacity-0">
            <option value="">Peu importe</option>
            <option value="action">Action</option>
            <option value="histoire">Histoire</option>
            <option value="drama">Drama</option>
            <option value="comédie">Comédie</option>
        </select>
    </div>
    <div class="select_wrapper text-gray-600 hover:text-gray-700 mx-3">
        <div class="select_overlay">
            <span class="mr-3"><i class="fas fa-chevron-down"></i></span>
            <span class="select_label" data-placeholder="Décennie">Décennie</span>
        </div>
        <select id="decade" name="decade" class="filter opacity-0">
            <option value="">Peu importe</option>
            <option value="2020">2020s</option>
            <option value="2010">2010s</option>
            <option value="2000">2000s</option>
            <option value="1990">1990s</option>
            <option value="1980">1980s</option>
            <option value="1970">1970s</option>
            <option value="1960">1960s</option>
            <option value="1950">1950s</option>
            <option value="1940">1940s</option>
            <option value="1930">1930s</option>
            <option value="1920">1920s</option>
            <option value="1910">1910s</option>
            <option value="1900">1900s</option>
        </select>
    </div> 
</form>
@if($films->count())
<table class="w-full mt-6">
    <thead class="text-gray-500 text-sm text-left">
        <tr>
            <th class="font-normal uppercase py-2">Titre</th>
            <th class="font-normal uppercase py-2">Année <span class="ml-2"><i class="fas fa-chevron-down"></i></span></th>
            <th class="font-normal uppercase py-2">Réalisé par</th>
            <th class="font-normal uppercase py-2">Genre</th>
            <th class="font-normal uppercase py-2">Tags</th>
            <th class="font-normal uppercase py-2">Streaming</th>
        </tr>
    </thead>
    <tbody class="mt-3">
        @foreach($films as $film)
        <tr>
            <td><a href="{{ route('film', $film->id) }}" class="border-b border-dashed p-1 border-black hover:bg-gray-200">{{ $film->title }}</a></td>
            <td>{{ $film->year }}</td>
            <td>{{ $film->directors }}</td>
            <td>{{ $film->genres }}</td>
            <td>{{ $film->tags }}</td>
            <td>{{ $film->stream_links }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="flex items-center justify-center text-gray-500 text-xl p-20">
    @if(request()->has('q') && request()->get('q'))
    <span class="mr-3"><i class="far fa-frown"></i></span> Aucun film trouvé pour la requête "<span class="mono">{{ request()->get('q') }}</span>".
    @else
    <span class="mr-3"><i class="fas fa-search"></i></span> Utilisez la barre en haut pour effectuer une recherche.
    @endif
</div>
@endif

@endsection