@extends('layout')

@section('content')
<h1 class="text-xl flex items-center uppercase mono">
    <span class="text-4xl text-mustard mr-2"><i class="fas fa-angle-right"></i></span>A propos
</h1>
<p class="mt-4 text-gray-800">
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque neque, nemo beatae architecto, voluptates non, quidem expedita laboriosam dolorum quibusdam adipisci asperiores molestiae ipsa tenetur minus. Iste porro eos animi!
</p>
<h2 class="text-lg mt-6 uppercase mono">
    <span class="text-2xl text-mustard mr-2"><i class="fas fa-angle-right"></i></span>Open source, open data
</h2>
<p class="text-gray-800 mt-3">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Pariatur neque in quisquam nostrum? Numquam, fugit? Quidem eos consectetur magni reiciendis inventore? Neque adipisci nemo accusamus, voluptate blanditiis quos temporibus suscipit.</p>
@endsection