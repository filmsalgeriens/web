@extends('layout')

@section('content')
<h1 class="text-xl flex items-center uppercase mono">
    <span class="text-4xl text-mustard mr-2"><i class="fas fa-angle-right"></i></span>Contact
</h1>
<p class="mt-4 text-gray-800">
    Si vous avez du feedback, des remarques ou vous voulez simplement dire bonjour, vous pouvez nous joindre via email: <a class="p-2 rounded-lg bg-gray-200 border border-gray-600 ml-2" href="mailto:contact@filmsalgeriens.com">contact@filmsalgeriens.com</a>
</p>
@endsection