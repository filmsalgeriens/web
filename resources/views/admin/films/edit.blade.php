@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header flex"><span class="flex-1">Modifier: {{ $film->title }}</span></div>

                <div class="card-body">
                    <form action="" class="flex flex-col" method="POST">
                        @csrf

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Titre <span class="text-red-700">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="title" required="required" class="form-control" value="{{ $film->title }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Titre original</label>
                            <div class="col-md-6">
                                <input type="text" name="original_title" class="form-control" value="{{ $film->original_title }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Année <span class="text-red-700">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="year" class="form-control" placeholder="e.g: 1999" value="{{ $film->year }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Durée <span class="ml-2 text-gray-600">(minutes)</span></label>
                            <div class="col-md-6">
                                <input type="text" name="runtime" class="form-control" value="{{ $film->runtime }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Pays</label>
                            <div class="col-md-6">
                                <input type="text" name="country" class="form-control" value="{{ $film->country }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Langues</label>
                            <div class="col-md-6">
                                <input type="text" name="languages" class="form-control" value="{{ $film->languages }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Date de sortie</label>
                            <div class="col-md-6">
                                <input type="text" name="release_date" class="form-control" placeholder="e.g: 1999-02-23" value="{{ $film->release_date }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <input type="text" name="film_type" class="form-control" value="{{ $film->film_type }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Genres</label>
                            <div class="col-md-6">
                                <input type="text" name="genres" class="form-control" value="{{ $film->genres }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Realisateur(s)</label>
                            <div class="col-md-6">
                                <input type="text" name="directors" class="form-control" value="{{ $film->directors }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Ecrivain(s)</label>
                            <div class="col-md-6">
                                <input type="text" name="writers" class="form-control" value="{{ $film->writers }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Acteurs</label>
                            <div class="col-md-6">
                                <input type="text" name="actors" class="form-control" value="{{ $film->actors }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Plot</label>
                            <div class="col-md-6">
                                <textarea name="plot" class="w-full border">{{ $film->plot }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Poster</label>
                            <div class="col-md-6">
                                <input type="text" name="poster_url" class="form-control" value="{{ $film->poster_url }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Cover</label>
                            <div class="col-md-6">
                                <input type="text" name="cover_url" class="form-control" value="{{ $film->cover_url }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Prix</label>
                            <div class="col-md-6">
                                <input type="text" name="awards" class="form-control" value="{{ $film->awards }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Lien streaming</label>
                            <div class="col-md-6">
                                <input type="text" name="stream_links" class="form-control" value="{{ $film->stream_links }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Tags</label>
                            <div class="col-md-6">
                                <input type="text" name="tags" class="form-control" value="{{ $film->tags }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Bande annonce</label>
                            <div class="col-md-6">
                                <input type="text" name="trailer" class="form-control" value="{{ $film->trailer }}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
