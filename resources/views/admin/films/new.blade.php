@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header flex"><span class="flex-1">Nouveau films</span></div>

                <div class="card-body">
                    <form action="" class="flex flex-col" method="POST" id="new-film-form">
                        @csrf
                        <h2 class="flex items-center mb-3"><span>Informations de base</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Titre <span class="text-red-700">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="title" required="required" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Titre original</label>
                            <div class="col-md-6">
                                <input type="text" name="original_title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Année <span class="text-red-700">*</span></label>
                            @php
                                $years = range(1800, now()->year+5);
                                arsort($years);
                            @endphp

                            <div class="col-md-6">
                                <select name="year" id="film-year">
                                    <option value="">Selectionner l'année</option>
                                    @foreach($years as $year)
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Durée <span class="ml-2 text-gray-600">(minutes)</span></label>
                            <div class="col-md-6">
                                <input type="text" name="runtime" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Pays</label>
                            @php
                                $countries = ["AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BQ", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CW", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SX", "SK", "SI", "SB", "SO", "ZA", "GS", "SS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"];
                            @endphp
                            <div class="col-md-6">
                                <select name="country" id="film-country">
                                    <option value="">Selectionner le pays</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country }}">{{ Locale::getDisplayRegion('en-'.$country, 'fr') }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Langues</label>
                            <div class="col-md-6">
                                <input type="text" name="languages" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Date de sortie</label>
                            <div class="col-md-6">
                                <input type="text" name="release_date" class="form-control" placeholder="e.g: 1999-02-23">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Type</label>
                            <div class="col-md-6">
                                <select name="type" class="" id="film-type">
                                    <option value="" selected="selected">Sélectionner un type</option>
                                    <option value="feature">Long-métrage (1h minimum, sorti dans les salles de cinéma)</option>
                                    <option value="telefilm">Téléfilm (concerne uniquement les téléfilms diffusés à la TV)</option>
                                    <option value="feature-animation">Long-métrage d'animation (film d'animation d'une durée supérieure à 60 minutes)</option>
                                    <option value="short">Court-métrage (film d'une durée maximum de 30 minutes)</option>
                                    <option value="short-animation">Court-métrage d'animation (film d'animation d'une durée maximum de 30 minutes)</option>
                                    <option value="mid">Moyen-métrage (film d'une durée maximum de 60 minutes)</option>
                                    <option value="mid-animation">Moyen-métrage d'animation (film d'animation d'une durée maximum de 60 minutes)</option>
                                    <option value="docu">Documentaire (documentaire sorti en salles uniquement)</option>
                                    <option value="docu-tv">Documentaire TV (réservé uniquement aux documentaires diffusés à la TV)</option>
                                    <option value="docu-animation">Documentaire d'animation (film documentaire d'animation d'une durée supérieure à 60 minutes)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Genres</label>
                            <div class="col-md-6">
                                <select name="genres" multiple id="film-genres">
                                    <option value="action">Action</option>
                                    <option value="animation">Animation</option>
                                    <option value="arts-martiaux">Arts martiaux</option>
                                    <option value="aventure">Aventure</option>
                                    <option value="biopic">Biopic</option>
                                    <option value="catastrophe">Catastrophe</option>
                                    <option value="comédie">Comédie</option>
                                    <option value="comédie-dramatique">Comédie dramatique</option>
                                    <option value="comédie-musicale">Comédie musicale</option>
                                    <option value="comédie-romantique">Comédie romantique</option>
                                    <option value="drame">Drame</option>
                                    <option value="épouvante-horreur">Épouvante-Horreur</option>
                                    <option value="érotique">Érotique</option>
                                    <option value="essai">Essai</option>
                                    <option value="expérimental">Expérimental</option>
                                    <option value="fantastique">Fantastique</option>
                                    <option value="fantasy">Fantasy</option>
                                    <option value="film-noir">Film noir</option>
                                    <option value="gangster">Gangster</option>
                                    <option value="guerre">Guerre</option>
                                    <option value="historique">Historique</option>
                                    <option value="jeunesse">Jeunesse</option>
                                    <option value="muet">Muet</option>
                                    <option value="musique">Musique</option>
                                    <option value="péplum">Péplum</option>
                                    <option value="policier">Policier</option>
                                    <option value="road-movie">Road movie</option>
                                    <option value="romance">Romance</option>
                                    <option value="science-fiction">Science-fiction</option>
                                    <option value="sketches">Sketches</option>
                                    <option value="sport">Sport</option>
                                    <option value="thriller">Thriller</option>
                                    <option value="western">Western</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Bande annonce</label>
                            <div class="col-md-6">
                                <input type="text" name="trailer" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Plot</label>
                            <div class="col-md-6">
                                <textarea name="plot" class="w-full border"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Lien streaming</label>
                            <div class="col-md-6">
                                <input type="text" name="stream_links" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Tags</label>
                            <div class="col-md-6">
                                <select name="tags" id="film-tags" multiple></select>
                            </div>
                        </div>
                        <h2 class="flex items-center mb-3"><span>Media</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="mb-10">
                            <h3 class="mb-2">Posters</h3>
                            <div id="film-posters" class="mb-4 bg-gray-200">
                                <div class="dz-message" data-dz-message><span>Choisissez des posters</span></div>
                            </div>
                            <a href="#" id="upload-posters" class="text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-upload"></i></span> Upload</a>
                        </div>
                        <div class="mb-10">
                            <h3 class="mb-2">Cover</h3>
                            <div id="film-covers" class="mb-4 bg-gray-200">
                                <div class="dz-message" data-dz-message><span>Choisissez un cover</span></div>
                            </div>
                            <a href="#" id="upload-covers" class="text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-upload"></i></span> Upload</a>
                        </div>
                        <h2 class="flex items-center mb-3"><span>Cast &amp; Crew</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Realisateur(s)</label>
                            <div class="col-md-6">
                                <select name="directors" id="film-directors" multiple></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Ecrivain(s)</label>
                            <div class="col-md-6">
                                <select name="writers" id="film-writers" multiple></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Acteurs</label>
                            <div class="col-md-6">
                                <select name="actors" id="film-actors" multiple></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Techniciens</label>
                            <div class="col-md-6">
                                <select name="technicians" id="film-technicians" multiple></select>
                            </div>
                        </div>
                        <h2 class="flex items-center mb-3"><span>Informations techniques</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Couleur</label>
                            <div class="col-md-6">
                                <select name="color" id="film-color">
                                    <option value="" selected>Selectionner...</option>
                                    <option value="bnw">Noir et blanc</option>
                                    <option value="color">Couleurs</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Son</label>
                            <div class="col-md-6">
                                <select name="sound" id="film-sound">
                                    <option value="">Choisissez</option>
                                    <option value="Mono">Mono</option>
                                    <option value="Stereo">Stereo</option>
                                    <option value="Silent">Silent</option>
                                    <option value="Dolby Digital">Dolby Digital</option>
                                    <option value="Dolby">Dolby</option>
                                    <option value="Dolby SR">Dolby SR</option>
                                    <option value="DTS">DTS</option>
                                    <option value="SDDS">SDDS</option>
                                    <option value="12-Track Digital Sound">12-Track Digital Sound</option>
                                    <option value="3 Channel Stereo">3 Channel Stereo</option>
                                    <option value="4-Track Stereo">4-Track Stereo</option>
                                    <option value="6-Track Stereo">6-Track Stereo</option>
                                    <option value="70 mm 6-Track">70 mm 6-Track</option>
                                    <option value="Afifa Ton-Kopie">Afifa Ton-Kopie</option>
                                    <option value="AGA Sound System">AGA Sound System</option>
                                    <option value="Animatophone">Animatophone</option>
                                    <option value="Auro 11.1">Auro 11.1</option>
                                    <option value="Auro 9.1">Auro 9.1</option>
                                    <option value="Aurofone">Aurofone</option>
                                    <option value="B.A.F. Sound System">B.A.F. Sound System</option>
                                    <option value="Blue Seal Noiseless Recording">Blue Seal Noiseless Recording</option>
                                    <option value="Bristolphone">Bristolphone</option>
                                    <option value="Broadway Surround">Broadway Surround</option>
                                    <option value="CDS">CDS</option>
                                    <option value="Chace Surround">Chace Surround</option>
                                    <option value="Chronophone">Chronophone</option>
                                    <option value="Cinematophone">Cinematophone</option>
                                    <option value="Cineophone">Cineophone</option>
                                    <option value="Cinephone">Cinephone</option>
                                    <option value="Cinerama 7-Track">Cinerama 7-Track</option>
                                    <option value="Cinesound">Cinesound</option>
                                    <option value="D-Cinema 48kHz 5.1">D-Cinema 48kHz 5.1</option>
                                    <option value="D-Cinema 48kHz 7.1">D-Cinema 48kHz 7.1</option>
                                    <option value="D-Cinema 48kHz Dolby Surround 7.1">D-Cinema 48kHz Dolby Surround 7.1</option>
                                    <option value="D-Cinema 96kHz 5.1">D-Cinema 96kHz 5.1</option>
                                    <option value="D-Cinema 96kHz 7.1">D-Cinema 96kHz 7.1</option>
                                    <option value="D-Cinema 96kHz Dolby Surround 7.1">D-Cinema 96kHz Dolby Surround 7.1</option>
                                    <option value="Datasat">Datasat</option>
                                    <option value="De Forest Phonofilm">De Forest Phonofilm</option>
                                    <option value="Digitrac Digital Audio System">Digitrac Digital Audio System</option>
                                    <option value="Dolby">Dolby</option>
                                    <option value="Dolby Atmos">Dolby Atmos</option>
                                    <option value="Dolby Digital">Dolby Digital</option>
                                    <option value="Dolby Digital EX">Dolby Digital EX</option>
                                    <option value="Dolby SR">Dolby SR</option>
                                    <option value="Dolby Stereo">Dolby Stereo</option>
                                    <option value="Dolby Surround 7.1">Dolby Surround 7.1</option>
                                    <option value="DTS">DTS</option>
                                    <option value="DTS 70 mm">DTS 70 mm</option>
                                    <option value="DTS-8">DTS-8</option>
                                    <option value="DTS-ES">DTS-ES</option>
                                    <option value="DTS-Stereo">DTS-Stereo</option>
                                    <option value="DX Stereo">DX Stereo</option>
                                    <option value="Filmtone">Filmtone</option>
                                    <option value="Full Range Recording System">Full Range Recording System</option>
                                    <option value="Gaumontphone">Gaumontphone</option>
                                    <option value="IMAX 6-Track">IMAX 6-Track</option>
                                    <option value="International Recording Engineers System">International Recording Engineers System</option>
                                    <option value="Iwerks Digital Audio">Iwerks Digital Audio</option>
                                    <option value="Kinetophone">Kinetophone</option>
                                    <option value="Kinopanorama 9-Track">Kinopanorama 9-Track</option>
                                    <option value="Kinoplasticon">Kinoplasticon</option>
                                    <option value="Klangfilm Magnetocord">Klangfilm Magnetocord</option>
                                    <option value="Klangfilm-Stereocord">Klangfilm-Stereocord</option>
                                    <option value="LC-Concept Digital Sound">LC-Concept Digital Sound</option>
                                    <option value="Li-Westrex System">Li-Westrex System</option>
                                    <option value="Magnaphone Western Electric">Magnaphone Western Electric</option>
                                    <option value="Magnetocord">Magnetocord</option>
                                    <option value="Matrix Surround">Matrix Surround</option>
                                    <option value="Mono">Mono</option>
                                    <option value="Optiphone">Optiphone</option>
                                    <option value="Ortiphone">Ortiphone</option>
                                    <option value="Perspecta Stereo">Perspecta Stereo</option>
                                    <option value="Phillips Sound">Phillips Sound</option>
                                    <option value="Phono-Cinema-Theatre">Phono-Cinema-Theatre</option>
                                    <option value="Phono-Kinema">Phono-Kinema</option>
                                    <option value="Phonofilm">Phonofilm</option>
                                    <option value="Pulvári System">Pulvári System</option>
                                    <option value="Quadrophonic">Quadrophonic</option>
                                    <option value="Quintaphonic">Quintaphonic</option>
                                    <option value="SDDS">SDDS</option>
                                    <option value="Sensurround">Sensurround</option>
                                    <option value="Servotron Stereo">Servotron Stereo</option>
                                    <option value="Silent">Silent</option>
                                    <option value="Sonics">Sonics</option>
                                    <option value="Sonics-DDP">Sonics-DDP</option>
                                    <option value="Sonix">Sonix</option>
                                    <option value="Sonora-Bristolphone">Sonora-Bristolphone</option>
                                    <option value="Sound 360°">Sound 360°</option>
                                    <option value="Sound Trax Surround Stereo">Sound Trax Surround Stereo</option>
                                    <option value="Soundelux">Soundelux</option>
                                    <option value="Spectra-Stereo">Spectra-Stereo</option>
                                    <option value="Stereo">Stereo</option>
                                    <option value="Super Space Sound">Super Space Sound</option>
                                    <option value="Synchrotone">Synchrotone</option>
                                    <option value="Synthetic">Synthetic</option>
                                    <option value="Systemi A. Shorin">Systemi A. Shorin</option>
                                    <option value="Système Cottet">Système Cottet</option>
                                    <option value="Tagephone">Tagephone</option>
                                    <option value="TMH Labs 10.2 Channel Sound">TMH Labs 10.2 Channel Sound</option>
                                    <option value="Tri-Ergon Sound System">Tri-Ergon Sound System</option>
                                    <option value="Ultra Stereo">Ultra Stereo</option>
                                    <option value="Variray Blue Seal Recording">Variray Blue Seal Recording</option>
                                    <option value="Visatone">Visatone</option>
                                    <option value="Vistasonic">Vistasonic</option>
                                    <option value="Vitaphone">Vitaphone</option>
                                    <option value="Vivaphone">Vivaphone</option>
                                    <option value="Wicmar and Blue Seal Noiseless Recording">Wicmar and Blue Seal Noiseless Recording</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Ratio</label>
                            <div class="col-md-6">
                                <select name="color" id="film-ratio">
                                    <option value="" selected>Selectionner...</option>
                                    <option value="16:9">16:9</option>
                                    <option value="1.33:1">1.33:1</option>
                                    <option value="1.37:1">1.37:1</option>
                                    <option value="1.66:1">1.66:1</option>
                                    <option value="1.78:1">1.78:1</option>
                                    <option value="1.85:1">1.85:1</option>
                                    <option value="2.35:1">2.35:1</option>
                                    <option value="4:3">4:3</option>
                                    <option value="other">Autre</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Caméra</label>
                            <div class="col-md-6">
                                <textarea name="camera" class="w-full border"></textarea>
                            </div>
                        </div>
                        <h2 class="flex items-center mb-3"><span>Productions</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="production">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Nom de la production</label>
                                <div class="col-md-6">
                                    <input type="text" name="production[0][name]" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Liens</label>
                                <div class="col-md-6">
                                    <select name="production[0][links]" class="production-links" multiple></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Détails (roles / ...)</label>
                                <div class="col-md-6">
                                    <textarea name="production[0][details]" class="w-full border"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4"></label>
                                <div class="col-md-6">
                                    <a href="#" class="add_production text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-plus"></i></span> Ajouter une production</a>
                                    <a href="#" class="ml-3 hidden delete_production text-red-600 hover:text-red-800"><span class="mr-2"><i class="fas fa-times"></i></span> Supprimer la production</a>
                                </div>
                            </div>
                        </div>

                        <h2 class="flex items-center mb-3"><span>Informations sur la production</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Budget</label>
                            <div class="col-md-6">
                                <input type="text" name="budget" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">ID IMDb</label>
                            <div class="col-md-6">
                                <input type="text" name="imdb" class="form-control" placeholder="e.g: tt0058946">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Lieux de tournage</label>
                            <div class="col-md-6">
                                <select name="locations" id="film-locations" multiple>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Dates de tournages</label>
                            <div class="col-md-6">
                                <select name="dates" id="film-dates" multiple>

                                </select>
                            </div>
                        </div>

                        <h2 class="flex items-center mb-3"><span>Prix</span> <span class="ml-2 border-b border-gray-500 flex-1"></span></h2>
                        <div class="festival">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Festival</label>
                                <div class="col-md-6">
                                    <input type="text" name="awards[0][festival]" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Prix</label>
                                <div class="col-md-6">
                                    <select name="awards[0][awards]" class="film-awards" multiple></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4"></label>
                                <div class="col-md-6">
                                    <a href="#" class="add_festival text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-plus"></i></span> Ajouter un festival</a>
                                    <a href="#" class="ml-3 hidden delete_festival text-red-600 hover:text-red-800"><span class="mr-2"><i class="fas fa-times"></i></span> Supprimer le festival</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
