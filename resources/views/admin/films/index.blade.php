@extends('layouts.app')

@section('assets')
    <script src="{{ mix('/build/js/admin.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header flex"><span class="flex-1">Films</span> <a href="{{ route('admin_create_film') }}">Ajouter</a></div>

                <div class="card-body">
                    @foreach($films as $film)
                    <div class="border m-2 p-2 flex">
                        <div class="flex-1">
                            {{ $film->title }} <span class="ml-2 text-gray-600">{{ $film->year }}</span>
                        </div>
                        <div class="ml-2">
                            <a href="{{ route('admin_edit_film', $film->id) }}" class="mr-3 text-indigo-700">Modifier</a>
                            <a href="{{ route('admin_delete_film', $film->id) }}" class="mr-2 text-red-700 delete_film">Supprimer</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
