@extends('layout')

@section('content')
<div class="flex">
    <div class="details">
        <h1 class="text-4xl">{{ $film->title }} ({{ $film->year }})</h1>
        <h3 class="mt-2 text-base uppercase">Film de {{ $film->directors }}</h3>
        
        <div class="mt-3 text-xs flex items-center">
            <span>{{ $film->genres }}</span>
            <span class="text-sm separator_icon mx-3"><i class="fas fa-circle"></i></span>
            <span>{{ $film->runtime }}</span>
        </div>
        
        <div class="mt-6 text-sm">{{ $film->plot }}</div>
        
        <div class="mt-10">
            <a href="{{ $film->trailer ? $film->trailer : '#' }}" class="uppercase p-2 bg-mustard text-xs mr-2 {{ !$film->trailer ? 'opacity-75' : '' }}">Bande-annonce</a>
            <a href="{{ $film->stream_links ? $film->stream_links : '#' }}" class="uppercase p-2 bg-mustard text-xs {{ !$film->stream_links ? 'opacity-75' : '' }}">Voir le film</a>
        </div>
    </div>
    <div class="poster w-64">
        @if($film->poster_url)
        <img src="{{ $film->poster_url }}" alt="{{ $film->title }}">
        @endif
    </div>
</div>


@endsection