<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FilmsAlgeriens</title>
    <link rel="stylesheet" href="{{ mix('build/css/base.css') }}">
    <script src="{{ mix('build/js/base.js') }}"></script>
</head>
<body>
    @if(!isset($_COOKIE['intro']))
    <div id="intro" class="bg-mustard text-gray-900 p-3 flex justify-center items-center">
        <p class="mr-4 text-sm">
            Ceci est un projet collaboratif, <a href="https://gitlab.com/filmsalgeriens/web" target="_blank" class="link">open source</a> et sans doute non-exhaustif. Toute aide, recommendation ou signalement d'une erreur sont les bienvenus. <a href="#" class="link">Cliquez ici</a> pour en savoir plus sur le projet.
        </p>
        <a href="#" class="dismiss hover:text-black text-md"><i class="far fa-times-circle"></i></a>
    </div>
    @endif
    <div id="container" class="flex flex-col">
        <header class="p-10 flex">
            <div class="flex flex-col">
                <a href="{{ route('home') }}" id="logo" class="font-bold text-2xl uppercase text-gray-900">FilmsAlgeriens.com</a>
                {{-- <p class="mono text-xs text-gray-800 w-64 leading-tight">
                    Plateforme répertoriant les films algériens de 1965 à nos jours
                </p> --}}
            </div>
            <nav class="ml-auto text-sm text-gray-600">
                <a href="{{ route('about') }}" class="{!! Request::is('about*') ? 'active' : '' !!}">à propos</a>
                <a href="{{ route('streaming') }}" class="{!! Request::is('streaming*') ? 'active' : '' !!}">streaming</a>
                <a href="{{ route('search') }}" class="{!! Request::is('search*') ? 'active' : '' !!}">trouver un film</a>
                <a href="{{ route('articles') }}" class="{!! Request::is('articles*') ? 'active' : '' !!}">articles</a>
                <a href="{{ route('contribute') }}" class="{!! Request::is('contribute*') ? 'active' : '' !!}">contribuer</a>
                <a href="{{ route('contact') }}" class="{!! Request::is('contact*') ? 'active' : '' !!}">contact</a>
            </nav>
        </header>
        <div id="main" class="flex-1 mt-8">
            @yield('content')
        </div>
    </div>
    
</body>
</html>