import $ from 'jquery';

import { dom, library } from '@fortawesome/fontawesome-svg-core';
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons/faTimesCircle";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons/faAngleRight";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons/faChevronDown";
import { faSearch } from "@fortawesome/free-solid-svg-icons/faSearch";
import { faFrown } from "@fortawesome/free-regular-svg-icons/faFrown";
import { faCircle } from "@fortawesome/free-solid-svg-icons/faCircle";


import { Cookie } from './utils';
var Turbolinks = require("turbolinks")


if(window.$ === undefined){
    window.$ = $;
}
if(window.jQuery === undefined){
    window.jQuery = $;
}


Turbolinks.start()
document.addEventListener("turbolinks:load", function(event) {
    // if(typeof(gtag) == 'function'){
    //     gtag('config', 'UA-167209985-1', {
    //         'page_title' : event.target.title,
    //         'page_path': event.data.url.replace(window.location.protocol + "//" + window.location.hostname, "")
    //     });
    // }

    library.add([faTimesCircle, faAngleRight, faChevronDown, faSearch, faFrown, faCircle]);
    dom.watch();

    // # Section: general
    $("body").on("click", "#intro .dismiss", function(e){
        e.preventDefault();

        let d = new Date();
        d.setDate(d.getDate() + 30 * 12);

        Cookie.write('intro', 1, d.toUTCString());

        $('#intro').hide();
    });


});
