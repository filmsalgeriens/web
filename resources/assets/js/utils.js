export class Utils {
    static isString(val) {
        return typeof val === 'string';
    }

    static isNumber(val) {
        return typeof val === 'number';
    }

    static isObject(val) {
        return val !== null && typeof val === 'object';
    }

}
export class Cookie {
    static write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (Utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (Utils.isString(path)) {
            cookie.push('path=' + path);
        }

        if (Utils.isString(domain)) {
            cookie.push('domain=' + domain);
        }

        if (secure === true) {
            cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
    }

    static read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
    }

    static remove(name) {
        this.write(name, '', Date.now() - 86400000);
    }
}
