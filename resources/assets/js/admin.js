import $ from 'jquery';

import { dom, library } from '@fortawesome/fontawesome-svg-core';
import { faPlus } from "@fortawesome/free-solid-svg-icons/faPlus";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { faUpload } from "@fortawesome/free-solid-svg-icons/faUpload";



if(window.$ === undefined){
    window.$ = $;
}
if(window.jQuery === undefined){
    window.jQuery = $;
}

$(function(){

    library.add([faPlus, faTimes, faUpload]);
    dom.watch();

    $("body").on("click", ".delete_film", function(e){
        let first_prompt = confirm('Etes-vous sur de vouloir supprimer ce film?');
        if(!first_prompt){
            e.preventDefault();
        }
        if(first_prompt && !confirm('Cette action est irréversible, ceci est le dernier avertissement!')){
            e.preventDefault();
        }
    });

    $('#film-type').select2({
        width: '100%',
    });
    $('#film-year').select2({
        width: '100%',
    });
    $('#film-country').select2({
        width: '100%',
    });
    $('#film-sound').select2({
        width: '100%',
    });

    $('#film-genres').select2({
        width: '100%',
        placeholder: 'Selectionner des genres (max. 4)',
        maximumSelectionLength: 4
    });

    $('#film-tags').select2({
        width: '100%',
        placeholder: 'Selectionner des tags',
        tags: true
    });
    $('#film-directors').select2({
        width: '100%',
        tags: true
    });
    $('#film-writers').select2({
        width: '100%',
        tags: true
    });
    $('#film-actors').select2({
        width: '100%',
        tags: true
    });
    $('#film-technicians').select2({
        width: '100%',
        tags: true
    });
    $('#film-color').select2({
        width: '100%',
    });
    $('#film-ratio').select2({
        width: '100%',
        tags: true
    });
    $('#film-locations').select2({
        width: '100%',
        placeholder: 'Lieux de tournage',
        tags: true
    });
    $('#film-dates').select2({
        width: '100%',
        placeholder: 'Dates de tournages',
        tags: true
    });
    $('.film-awards').select2({
        width: '100%',
        tags: true
    });

    $("body").on("click", ".add_festival", function(e){
        let index = Math.round(new Date().getTime()/1000);
        $(this).addClass('hidden');

        $(`<div class="festival border-t mt-3 pt-3">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">Festival</label>
                <div class="col-md-6">
                    <input type="text" name="awards[${index}][festival]" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">Prix</label>
                <div class="col-md-6">
                    <select name="awards[${index}][awards]" class="film-awards" multiple></select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4"></label>
                <div class="col-md-6">
                    <a href="#" class="add_festival text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-plus"></i></span> Ajouter un festival</a>
                    <a href="#" class="ml-3 delete_festival text-red-600 hover:text-red-800"><span class="mr-2"><i class="fas fa-times"></i></span> Supprimer festival</a>
                </div>
            </div>
        </div>`).insertAfter('.festival:last').find('select').select2({
            width: '100%',
            tags: true
        });

        $('.delete_festival').removeClass('hidden');

        e.preventDefault();
    });
    
    $("body").on("click", ".delete_festival", function(e){
        $(this).parents('.festival').remove();
        if($('.festival').length == 1){
            $('.festival .delete_festival').addClass('hidden');
            $('.festival .add_festival').removeClass('hidden');
        }

        e.preventDefault();
    });

    $('.production-links').select2({
        width: '100%',
        tags: true
    });

    $("body").on("click", ".add_production", function(e){
        let index = Math.round(new Date().getTime()/1000);
        $(this).addClass('hidden');

        $(`<div class="production border-t mt-3 pt-3">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">Nom de la production</label>
                <div class="col-md-6">
                    <input type="text" name="production[${index}][name]" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">Liens</label>
                <div class="col-md-6">
                    <select name="production[${index}][links]" class="production-links" multiple></select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">Détails (roles / ...)</label>
                <div class="col-md-6">
                    <textarea name="production[${index}][details]" class="w-full border"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4"></label>
                <div class="col-md-6">
                    <a href="#" class="add_production text-indigo-600 hover:text-indigo-800"><span class="mr-2"><i class="fas fa-plus"></i></span> Ajouter une production</a>
                    <a href="#" class="ml-3 hidden delete_production text-red-600 hover:text-red-800"><span class="mr-2"><i class="fas fa-times"></i></span> Supprimer la production</a>
                </div>
            </div>
        </div>`).insertAfter('.production:last').find('select').select2({
            width: '100%',
            tags: true
        });

        $('.delete_production').removeClass('hidden');

        e.preventDefault();
    });

    $("body").on("click", ".delete_production", function(e){
        $(this).parents('.production').remove();

        if($('.production').length == 1){
            $('.production .delete_production').addClass('hidden');
            $('.production .add_production').removeClass('hidden');
        }

        e.preventDefault();
    });

    let posters = new Dropzone('#film-posters', {
        url: '/home/films/posters',
        addRemoveLinks: true,
        autoProcessQueue: false,
        paramName: 'poster',
        previewTemplate: `
        <div class="dz-preview dz-image-preview">
            <div class="dz-image"><img data-dz-thumbnail></div>
            <div class="dz-details">
                <div class="dz-size"><span data-dz-size></span></div>
                <div class="dz-filename"><span data-dz-name></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
            <div class="dz-error-message"><span data-dz-errormessage></span></div>
            <div class="dz-success-mark"></div>
            <div class="dz-error-mark"></div>
            <div>
                <input type="text" class="poster-caption p-2 m-1 border" placeholder="caption">
            </div>
            <a class="dz-remove" data-dz-remove=""></a>
        </div>`,
    });

    $('#film-posters').addClass('dropzone');

    let csrf = $('meta[name="csrf-token"]').attr('content');

    posters.on("sending", function(file, xhr, formData) {
        formData.append("caption", file.previewElement.querySelector('.poster-caption').value);
        formData.append("_token", csrf);
    });

    posters.on("success", function(file, res){
        let index = $('.poster-input').length;

        $(`<div class="poster-input">
            <input type="hidden" name="posters[${index}][path]" value="${res}">
            <input type="hidden" name="posters[${index}][caption]" value="${file.previewElement.querySelector('.poster-caption').value}">
        </div>`).appendTo('#new-film-form');
    });

    $("body").on("click", "#upload-posters", function(e){
        posters.processQueue();

        e.preventDefault();
    });


    let covers = new Dropzone('#film-covers', {
        url: '/home/films/covers',
        addRemoveLinks: true,
        autoProcessQueue: false,
        paramName: 'cover',
        previewTemplate: `
        <div class="dz-preview dz-image-preview">
            <div class="dz-image"><img data-dz-thumbnail></div>
            <div class="dz-details">
                <div class="dz-size"><span data-dz-size></span></div>
                <div class="dz-filename"><span data-dz-name></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
            <div class="dz-error-message"><span data-dz-errormessage></span></div>
            <div class="dz-success-mark"></div>
            <div class="dz-error-mark"></div>
            <div>
                <input type="text" class="cover-caption p-2 m-1 border" placeholder="caption">
            </div>
            <a class="dz-remove" data-dz-remove=""></a>
        </div>`,
    });

    $('#film-covers').addClass('dropzone');

    covers.on("sending", function(file, xhr, formData) {
        formData.append("caption", file.previewElement.querySelector('.cover-caption').value);
        formData.append("_token", csrf);
    });

    covers.on("success", function(file, res){
        let index = $('.cover-input').length;

        $(`<div class="cover-input">
            <input type="hidden" name="covers[${index}][path]" value="${res}">
            <input type="hidden" name="covers[${index}][caption]" value="${file.previewElement.querySelector('.cover-caption').value}">
        </div>`).appendTo('#new-film-form');
    });

    $("body").on("click", "#upload-covers", function(e){
        covers.processQueue();

        e.preventDefault();
    });



})


