<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConvertUrlColumnsToTextInFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->text('poster_url')->nullable()->change();
            $table->text('trailer_url')->nullable()->change();
            $table->text('cover_url')->after('poster_url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->string('poster_url')->nullable()->change();
            $table->string('trailer_url')->nullable()->change();
            $table->string('cover_url')->after('poster_url')->nullable()->change();
        });
    }
}
