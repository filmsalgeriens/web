<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('original_title')->nullable();
            $table->smallInteger('year');
            $table->smallInteger('runtime')->nullable();
            $table->string('country')->nullable();
            $table->string('languages')->nullable();
            $table->date('release_date')->nullable();
            $table->string('film_type')->nullable();
            $table->string('genres')->nullable();
            $table->text('directors')->nullable();
            $table->text('writers')->nullable();
            $table->text('actors')->nullable();
            $table->longText('plot')->nullable();
            $table->string('poster_url')->nullable();
            $table->text('awards')->nullable();
            $table->text('stream_links')->nullable();
            $table->text('tags')->nullable();
            $table->string('trailer_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
