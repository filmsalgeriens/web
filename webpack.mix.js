const mix = require('laravel-mix');
const tailwind = require('tailwindcss')({
    purge: [
        './resources/views/**/*.blade.php',
        './resources/views/**/*.html',
        './resources/assets/**/*.js',
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const css_plugins = [
    tailwind,
    require('postcss-apply')
];

mix.version();

mix.copy('resources/assets/js/lazysizes.min.js', 'public/build/js');
mix.js('resources/assets/js/base.js', 'public/build/js/base.js');
mix.js('resources/assets/js/admin.js', 'public/build/js/admin.js');

mix.postCss('resources/assets/css/base.css', 'public/build/css', css_plugins);

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');




