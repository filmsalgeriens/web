<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function films(Request $request){
        $films = Film::all();

        return view('admin.films.index')->with(compact('films'));
    }

    public function create_film(Request $request){
        return view('admin.films.new');
    }

    public function save_film(Request $request){

        Film::updateOrCreate([
            'title' => $request->title,
            'original_title' => $request->original_title,
            'year' => $request->year,
            'runtime' => $request->runtime,
            'country' => $request->country,
            'languages' => $request->languages,
            'release_date' => $request->release_date,
            'film_type' => $request->film_type,
            'genres' => $request->genres,
            'directors' => $request->directors,
            'writers' => $request->writers,
            'actors' => $request->actors,
            'plot' => $request->plot,
            'poster_url' => $request->poster_url,
            'cover_url' => $request->cover_url,
            'awards' => $request->awards,
            'stream_links' => $request->stream_links,
            'tags' => $request->tags,
            'trailer_url' => $request->trailer_url,
        ]);

        return redirect()->route('admin_films');
    }

    public function edit_film(Request $request, $film_id){
        $film = Film::find($film_id);

        if(!$film){
            return redirect()->route('admin_films');
        }

        return view('admin.films.edit')->with(compact('film'));
    }

    public function update_film(Request $request, $film_id){

        $film = Film::find($film_id);

        if($film){
            $film->title = $request->title;
            $film->original_title = $request->original_title;
            $film->year = $request->year;
            $film->runtime = $request->runtime;
            $film->country = $request->country;
            $film->languages = $request->languages;
            $film->release_date = $request->release_date;
            $film->film_type = $request->film_type;
            $film->genres = $request->genres;
            $film->directors = $request->directors;
            $film->writers = $request->writers;
            $film->actors = $request->actors;
            $film->plot = $request->plot;
            $film->poster_url = $request->poster_url;
            $film->cover_url = $request->cover_url;
            $film->awards = $request->awards;
            $film->stream_links = $request->stream_links;
            $film->tags = $request->tags;
            $film->trailer_url = $request->trailer_url;
            $film->save();

            return redirect()->route('admin_edit_film', $film->id);
        }else{
            return redirect()->route('admin_films');
        }
    }

    public function delete_film(Request $request, $film_id){
        $film = Film::find($film_id);

        if($film){
            $film->delete();
        }

        return redirect()->route('admin_films');
    }

    public function upload_posters(Request $request){
        return $request->file('poster')->store('public/films/posters');
    }

    public function upload_covers(Request $request){
        return $request->file('cover')->store('public/films/covers');
    }
}
