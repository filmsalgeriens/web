<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;

class BaseController extends Controller
{
    public function index(Request $request){
        return view('index');
    }

    public function about(Request $request){
        return view('about');
    }

    public function streaming(Request $request){
        return view('streaming');
    }

    public function search(Request $request){
        $films = collect([]);

        if($request->has('q')){
            $films = Film::where('title', 'like', '%'.$request->get('q').'%')->get();
        }

        return view('search')->with(compact('films'));
    }

    public function articles(Request $request){
        return view('articles');
    }

    public function contribute(Request $request){
        return view('contribute');
    }

    public function contact(Request $request){
        return view('contact');
    }

    public function film($film_id, $slug = null){
        $film = Film::find($film_id);

        return view('film')->with(compact('film'));
    }
}
