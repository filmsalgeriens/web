<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BaseController@index')->name('home');
Route::get('about', 'BaseController@about')->name('about');
Route::get('streaming', 'BaseController@streaming')->name('streaming');
Route::get('search', 'BaseController@search')->name('search');
Route::get('articles', 'BaseController@articles')->name('articles');
Route::get('contribute', 'BaseController@contribute')->name('contribute');
Route::get('contact', 'BaseController@contact')->name('contact');

Route::get('/films/{id}', 'BaseController@film')->name('film');

Auth::routes(['register' => false]);

Route::middleware('auth')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/films', 'HomeController@films')->name('admin_films');
    Route::get('/home/films/new', 'HomeController@create_film')->name('admin_create_film');
    Route::post('/home/films/new', 'HomeController@save_film');
    Route::get('/home/films/{id}/edit', 'HomeController@edit_film')->name('admin_edit_film');
    Route::post('/home/films/{id}/edit', 'HomeController@update_film');
    Route::post('/home/films/posters', 'HomeController@upload_posters');
    Route::post('/home/films/covers', 'HomeController@upload_covers');
    Route::get('/home/films/{id}/delete', 'HomeController@delete_film')->name('admin_delete_film');
});


